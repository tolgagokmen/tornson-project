{
	"title": "Mains",
	"introduction" : "The main courses of my career. This is how I've spent most of my days. Here's where my <a href='/starters'>starters</a> really paid off, and are enriched by what's happening on a day to day basis. Every day. I'll mention a few of the tools and tricks I've used - but it won't be all. But by all means, scroll down and have a look at what I've been doing all these years!",
	"items": [
	{
			"title": "Nu.nl - Sanoma",
			"subtitle": "Software developer",
			"year": "2013 - current",
			"image": "/static/images/ws_nu.jpg",
			"content": [
				"<p>Working every waking moment for a football stadium full of people. A <b>b</b>illion viewers a ",
                "month. That's your audience. In and on a complex microservices based architecture, depending on a ",
                "lot of  external data resources. A fast moving period. The only thing constant is change. We've ",
                "rebuilt nu.nl to a fully responsive, API-based frontend. We've built nutech.nl, rebuilt nusport.nl ",
                " and wtf.nl . And due to policy change, we've integrated all ",
                " content of nutech.nl, nusport.nl and nuzakelijk.nl in to Nu.nl. We've started the move from ",
                " Apache/mod_wsgi to NGINX/uwsgi stack. </p>",
                "<p>A few highlights: improved monitoring. ",
                " Automated documentation. Automated builds. Puppetized our (main) ",
                " assets. Migrated lots of content. Really, a lot of content. </p>"
			],
			"tools" : [
				"Python/Django",
				"PHP/ATK",
		        "Puppet",
				"Ansible",
				"Terraform",
		        "MySQL",
		        "New Relic",
		        "Sentry",
		        "Bamboo",
		        "ElasticSearch/LogStash"
			]

		},
		{
			"title": "Sanoma Account",
			"subtitle": "Software developer/Solution Architect",
			"year": "june 2015 - december 2015",
			"image": "/static/images/ws_account.jpg",
			"content": [
				"<p>Integration of 3rd User Identity Management System on all Sanoma sites. Generic, yet flexible and customizable. ",
				"The frontend solution should be implemented on all apps and websites - often replacing the regular user registration, ",
				" yet it has to play nice with the rest of the web application(s). And of course, all data is to be integrated with Sanoma Business Systems.</p>",
				"<p>Development/architecture of centralized management environment for the used screensets and their implementations. Development/review of the frontend implementation </p>"
			],
			"tools" : [
				"Python/Django",
				"Puppet",
				"New Relic",
		        "Sentry",
		        "Bamboo",
		        "Gigya"
			]

		},
		{
			"title": "Online Women - Sanoma",
			"subtitle" : "Lead Software Developer ",
			"year": "2011 - 2013",
			"image": "/static/images/ws_online_vrouwen.jpg",
			"content": [
                "<p>Development of sites like Viva, Libelle, Margriet, as well as Styletoday.nl, Vrouwonline.nl. ",
                "Contentsites, and a lot of them with a very active user forum. </p>",
                "<p>As team lead, I had to make sure that in a time of constant change, the team was able to ",
                " proceed with business as usual. Also, I did main development of online diet platform Stepaday.nl.</p>"
			],
			"tools" : [
				"Python/Django",
		        "PHP",
		        "MySQL",
		        "Sentry",
		        "Jenkins"
			]
		},
		{
			"title": "Strawberries ",
			"subtitle": "Software Developer",
			"year": "2008 - 2011",
			"image": "/static/images/ws_strawberries.jpg",
			"content": [
		        "<p>The Full Service Internet Bureau Strawberries (now: Osudio) had developed their own Content ",
                " Management System. Php based, and loaded with a postgres backend. It was the go to place for content",
                " sites developed in that time. </p>",
                "<p> A few sites we did there: eurolines, design.nl, compass-group, kangaroos, flip-flop.de </p>"
			],
			"tools" : [
				"PHP",
		        "PostgreSQL",
		        "MySQL",
		        "Magento"
			]
		},
		{
			"title": "Araneum ",
			"subtitle": "Software Developer",
			"year": "2005 - 2008",
			"image": "/static/images/ws_ara.jpg",
			"content": [
		        "<p>A small internet bureau with their own web to print platform solution. The platform was developed ",
                "in house, as well as quite a few websites.</p>",
                "<p>Among a lot of other things: worked on an online tool with which you could create your own comic. ",
                "</p>"
			]

		}
	]
}
