{
	"title": "Starters",
	"introduction": "<p>An educated set of starters. I will not include here that I started programming on our family's MSX at age 10. I will not bother you with a lot of smaller courses I did. All the conferences I've attended? Never mind. We'll cross that bridge when we get there.</p><p>Learning by doing, that is my forte. When you look at the <a href='/mains'>mains</a>, you'll have a more in depth view of my coding background.</p>",
	"items" : [
		{
			"title": "MSc Bioprocess Engineering",
			"subtitle": "Wageningen University",
			"year": "1996 - 2005",
            "image": "/static/images/ws_wur.jpg",
			"content":
				[
					"<p>Let's have a look at wine. Wine is made of grapes, that are squashed and fermented. ",
					"For that fermentation, you need yeast. Yeast is a living thing, it eats, grows and multiplies. ",
					"And it rewards you with alcohol as a Thank You For Feeding Me.<br/>",
					"But, when you want to make lots of wine, you need to use lots of yeast. And you'll have to ",
					"feed them, all equally. That's a complete different science than feeding and breeding them ",
					"in the first place.</p>",
                    "<p>That's what this education is about. Both the microbiological and ",
					"the process engineering side. And of course, not only wine. Think biofuels. ",
					"Think Functional Foods. Think pharmaceuticals,  for example the substances that are retrieved ",
                    "from sponges.</p>",
                    "<p>Apart from the academic mindset, the thing I learned most here? Scaling up is not just ",
                    "opening up the tap.</p>",
					"<h3>Thesises:</h3>",
					"<ul class='list'><li><h5>Design of digital educational material: extraction</h5></li>",
					"<li><h5>Control of spatial distributed qualities in model food</h5></li></ul>",
					"<h3>Internship</h3>",
					"<h5>Creating hydrogen gas by vaporising biomass in supercritical water</h5>"
				]
		},
		{
			"title": "VWO",
			"subtitle": "Collegium Marianum Venlo",
			"year": "1989 - 1996",
            "image": "/static/images/ws_marianum.jpg",
			"content":
				[
					"<p>According to Wikipedia, it is a <a href='http://en.wikipedia.org/wiki/Voorbereidend_wetenschappelijk_onderwijs'>pre-university secondary education.</a> </p>"
				]
		}
	]
}