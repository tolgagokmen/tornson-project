var header_container = '.header-container';
var shrinkOn = 300;
var header;

$(document).ready(function() {
    $(document).scroll(function(){
        var distanceY = jQuery(this).scrollTop(),
            header = $(header_container);
        if (distanceY > shrinkOn) {
            header.addClass('smaller');
        } else {
            if (header.hasClass("smaller")) {
                header.removeClass('smaller');
            }
        }
    });
});