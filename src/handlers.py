__author__ = 'bartstroeken'
from tornado import httpclient
from backends.jsonbackend import JSONFileContentReader
from mako import exceptions
from mako.lookup import TemplateLookup

import tornado.web
from settings import *

template_lookup = TemplateLookup(input_encoding='utf-8',
                                 output_encoding='utf-8',
                                 encoding_errors='replace',
                                 directories=[template_root],
                                 strict_undefined=True,
                                 format_exceptions=True)

class BaseHandler(tornado.web.RequestHandler):
    """
    Base handler gonna to be used instead of RequestHandler
    """
    def write_error(self, status_code, **kwargs):
        if status_code in [403, 404, 500, 503]:
            error = 'Error %s' % status_code
        else:
            error = 'Damn. That is something different.'
        kwargs = {'error': error, 'content':{'title': error}}
        template_content = template_lookup.get_template('not_found.html')
        self.write(template_content.render(**kwargs))


class ErrorHandler(tornado.web.ErrorHandler, BaseHandler):
    """
    Default handler gonna to be used in case of 404 error
    """
    pass


class MainHandler(BaseHandler):
    def data_received(self, chunk):
        pass

    def get(self, contentname=False):
        """
        First of all, try to retrieve the data of
        data/{contentname}.js
        and if it exists, try to render
        templates/{contentname}.html
        """
        if not contentname:
            contentname = default_contentname

        menureader = JSONFileContentReader(name='menu')
        contentreader = JSONFileContentReader(name=contentname)
        # get content
        content = contentreader.get_content()
        menu = menureader.get_content()

        if not menu or not content:
            return httpclient.HTTPError(404)

        # render template
        template = self.get_template_for_content(contentname)
        try:
            template_content = template_lookup.get_template(template)
            kwargs = {'menu': menu, 'content': content, 'contentname': contentname}
            self.write(template_content.render(**kwargs))
        except exceptions.TopLevelLookupException:
            raise httpclient.HTTPError(404)

    @staticmethod
    def get_template_for_content(contentname):
        """
        Get the valid template name for the content

        """
        filename = contentname + '.html'
        if os.path.isfile(os.path.join(template_root, filename)):
            return filename
        return default_template

    def get_error_html(self, status_code, exception, **kwargs):
        if hasattr(exception, 'code'):
            self.set_status(exception.code)
            if exception.code == 500:
                return exceptions.html_error_template().render()
            return self.render_template(str(exception.code))
        return exceptions.html_error_template().render()

