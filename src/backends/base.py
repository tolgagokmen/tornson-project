__author__ = 'bartstroeken'


class BaseContentReader(object):

    def __init__(self, name, *args, **kwargs):
        """
        ContentReader
        :param name:
        :param args:
        :param kwargs:
        :return:
        """
        self.name = name

    def validate(self, *args, **kwargs):
        """
        Validate the content

        :return: boolean
        """
        return True

    def get_content(self, *args, **kwargs):
        return self.validate()

