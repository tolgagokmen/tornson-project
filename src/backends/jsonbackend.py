__author__ = 'bartstroeken'
import json
import re
from settings import *
from base import BaseContentReader
from collections import OrderedDict

# http://stackoverflow.com/questions/8730119/retrieving-json-objects-from-a-text-file-using-python
# shameless copy paste from json/decoder.py
FLAGS = re.VERBOSE | re.MULTILINE | re.DOTALL
WHITESPACE = re.compile(r'[ \t\n\r]*', FLAGS)


class JSONFileContentReader(BaseContentReader):
    """
    JSONFileContentReader
    Reads the content from a JSON-file.
    The file is retrieved from a set dir.

    """

    contentfile = False

    file_extension = 'js'

    def validate(self, *args, **kwargs):
        """
        Validate the content.
        :param args:
        :param kwargs:
        :return:
        """
        json_file = os.path.join(data_root, self.name + '.' + self.file_extension)
        self.contentfile = JSONReader(json_file)

        if not self.contentfile.valid:
            return False
        return True

    def get_content(self, *args, **kwargs):
        """
        Get the content from the file.

        :param args:
        :param kwargs:
        :return:
        """
        valid = super(JSONFileContentReader, self).get_content(*args, **kwargs)
        if not valid:
            return False
        return self.contentfile.get_content()


class JSONReader(object):

    def __init__(self, filepath):
        self.valid = False
        if os.path.isfile(filepath):
            self.valid = True
            self.filepath = filepath

    def get_content(self):
        """
        Get the content from the given file, if applicable
        :return:
        """
        if not self.valid:
            return False

        filepointer = open(self.filepath)
        content = json.loads(filepointer.read(), cls=ConcatJSONDecoder, object_pairs_hook=OrderedDict)
        filepointer.close()

        result = content.pop()
        end_result = self.flatten_content(result)
        ordered_result = OrderedDict(end_result)
        return ordered_result

    @classmethod
    def flatten_content(cls, content):
        """
        Sometimes, a content key is too long to fit a line. If so, add it to the items_to_flatten dict,
        which will.... flatten the content into a single line string. 

        :param: dict content
        :return: OrderedDict
        """
        items_to_flatten = content.get('items_to_flatten', [])
        res = {}
        for key, value in content.iteritems():
            if key in items_to_flatten:
                value = ''.join(value)

            res.update({key: value})

        return res


class ConcatJSONDecoder(json.JSONDecoder):

    def decode(self, s, _w=WHITESPACE.match):
        """
        Decode JSON content

        """
        s_len = len(s)

        objs = []
        end = 0
        while end != s_len:
            obj, end = self.raw_decode(s, idx=_w(s, end).end())
            end = _w(s, end).end()
            objs.append(obj)

        return objs
