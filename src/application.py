#!/usr/bin/env python
# TORNADO
## -*- coding: utf-8 -*-

import tornado.ioloop
from handlers import MainHandler
from settings import *

application = tornado.web.Application([
    (r'^/(.*)$', MainHandler),
], debug=debug, static_path=os.path.join(root, 'static'))

if __name__ == '__main__':
    if not port:
        port = 8080
    application.listen(8080)
    tornado.ioloop.IOLoop.instance().start()
