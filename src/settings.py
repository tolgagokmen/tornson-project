__author__ = 'bartstroeken'

import os

root = os.path.dirname(__file__)
template_root = os.path.join(root, 'templates')

default_contentname = 'chefs_note'
default_template = 'base.html'

blacklist_templates = ('layouts',)
blacklist_data = ('menu',)

data_root = os.path.join(root, 'data')

debug = True
port = 80