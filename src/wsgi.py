__author__ = 'bartstroeken'

# Step 1 - properly activate the virtualenv
import os
import site

LOCAL_PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

site.addsitedir(LOCAL_PROJECT_ROOT)

activation_path = os.path.join(LOCAL_PROJECT_ROOT, '../venv/bin/activate_this.py')
execfile(activation_path, dict(__file__=activation_path))

# Step 2 - import and execute the handlers
from handlers import MainHandler
from settings import *
import tornado.web
import tornado.wsgi

application = tornado.web.Application([
    (r'^/(.*)$', MainHandler),
], debug=debug, static_path=os.path.join(root, 'static'))

application = tornado.wsgi.WSGIAdapter(application)

